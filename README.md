# TODO
- [ ] Services need to consume the JWT token
- [ ] Services need to consume the config service
- [ ] Unit tests
- [ ] Integration tests (?)
- [ ] Proper CI / CD

# Deployment
This section is a step by step guide to setup Spotify Light

### Step 1: Clone the repository
``` 
git clone git@gitlab.com:spotify-light-itsm/Main.git
``` 
### Step 2: Change directory and pull the latest images
```
cd ./Main/
docker-compose pull
``` 
### Step 3: Start Spotify Light
#### Recommended for testing:
``` 
docker-compose up 
``` 
#### Recommended for production:
This will start the application in detached mode
``` 
docker-compose up -d
``` 

# API Documentation
The API docs can be found [here](https://gitlab.com/spotify-light-itsm/Main/-/blob/master/docs/SwaggerDocu.yaml).
